CLI_CONFIG = {
    "output": {
        "source": "rend",
        "default": None,
    },
    "ref": {"positional": True},
}

CONFIG = {
    "ref": {
        "type": str,
        "help": "The resource on the hub for which to find docs",
        "default": None,
    },
}

DYNE = {}
